package chili.nm.test.runner;

import com.codeborne.selenide.*;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.InputStream;

public class BaseTest {
private void initializationSelenide() {

        Configuration.headless = true;
    Configuration.browserSize = "1920x1080";
    Configuration.pageLoadTimeout = 30000;
    Configuration.timeout = 30000;

    }

    // static boolean isServerRun() {
    //     return System.getenv("CI_RUN") != null;}

    // private static void initProperties() {
    //     if (isServerRun()) {
    //         Configuration.headless = true;
    //         Configuration.browserSize = "1920x1080";
    //     }else {
    //         InputStream inputStream = BaseTest.class.getClassLoader().getResourceAsStream("selenide.properties");
    //         if (inputStream == null) {
    //             System.out.println("ERROR: The \u001B[31mselenide.properties\u001B[0m file not found in src/test/resources/ directory.");
    //             System.out.println("You need to create it from selenide.properties.TEMPLATE file.");
    //             System.exit(1);
    //         }
    //     }
// }
    public void closeDriver() {
        Driver driver = WebDriverRunner.driver();
        if (driver.hasWebDriverStarted()) {
            driver.close();
        }
    }

    @BeforeClass
    public void setUp() {
//         initProperties();
        initializationSelenide();
    }

    @AfterClass
    public void down() {
    closeDriver();
    }
}
